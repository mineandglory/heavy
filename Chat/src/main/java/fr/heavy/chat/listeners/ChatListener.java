package fr.heavy.chat.listeners;

import fr.heavy.core.api.translation.Translation;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener
{
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event)
    {
        String message = event.getMessage();
        message = Translation.getTranslation(event.getPlayer(), "chatFormat", message);

        event.setFormat(message);
    }
}
