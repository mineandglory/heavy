package fr.heavy.chat;

import fr.heavy.chat.listeners.ChatListener;
import fr.heavy.core.api.config.Config;
import fr.heavy.core.api.database.Database;
import fr.heavy.core.api.translation.Translation;
import org.bukkit.plugin.java.JavaPlugin;

public class ChatPlugin extends JavaPlugin
{
    private static JavaPlugin instance;

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;

        createDefaultConfig();

        Translation.loadTranslation(this);

        getServer().getPluginManager().registerEvents(new ChatListener(), this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
        Database.disconnectDatabase();
    }

    public static JavaPlugin getInstance()
    {
        return instance;
    }

    private void createDefaultConfig() {
        if (!Config.configExists(this, "translations/EN_US.yml"))
            Config.saveResource(this, "EN_US.yml", "translations/EN_US.yml");
    }
}
