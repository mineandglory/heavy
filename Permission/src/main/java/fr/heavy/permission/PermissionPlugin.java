package fr.heavy.permission;

import fr.heavy.core.api.config.Config;
import fr.heavy.core.api.database.Database;
import fr.heavy.core.api.placeholder.Placeholder;
import fr.heavy.core.api.translation.Translation;
import fr.heavy.permission.commands.GroupCommand;
import fr.heavy.permission.group.Group;
import fr.heavy.permission.listeners.ConnectionListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class PermissionPlugin extends JavaPlugin
{
    private static JavaPlugin instance;

    public static JavaPlugin getInstance()
    {
        return instance;
    }


    @Override
    public void onEnable()
    {
        super.onEnable();
        instance = this;

        createDefaultConfig();

        Translation.loadTranslation(this);
        createDefaultDatabase();

        Group.loadGroups();

        restoreUsers();

        getServer().getPluginManager().registerEvents(new ConnectionListener(), this);

        getCommand("group").setExecutor(new GroupCommand());

        new Placeholder("group", player ->
        {
            PermissionUser permissionUser = PermissionUser.getUserByUUID(player.getUniqueId());

            return permissionUser.getGroup().getDisplayname();
        });
    }

    @Override
    public void onDisable()
    {
        super.onDisable();
        for (Player player : Bukkit.getOnlinePlayers())
        {
            PermissionUser.getUserByUUID(player.getUniqueId()).removePermissions(player);
        }
    }

    private void restoreUsers()
    {
        for (Player player : Bukkit.getOnlinePlayers())
        {
            PermissionUser.loadUser(player.getUniqueId()).applyPermissions(player);
        }
    }

    private void createDefaultDatabase()
    {
        Database.executeUpdate("CREATE TABLE IF NOT EXISTS tb_group(" +
                "grp_uuid VARCHAR(36) PRIMARY KEY," +
                "grp_group VARCHAR(36)" +
                ");");

        Database.executeUpdate("CREATE TABLE IF NOT EXISTS tb_permission(" +
                "perm_uuid VARCHAR(36) PRIMARY KEY," +
                "perm_permission VARCHAR(64)" +
                ");");
    }

    private void createDefaultConfig()
    {
        if (!Config.configExists(this, "translations/EN_US.yml"))
            Config.saveResource(this, "EN_US.yml", "translations/EN_US.yml");

        if (!Config.configExists(this, "groups.yml"))
            Config.saveResource(this, "groups.yml", "groups.yml");
    }
}
