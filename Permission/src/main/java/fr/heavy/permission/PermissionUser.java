package fr.heavy.permission;

import fr.heavy.core.api.database.Database;
import fr.heavy.core.api.database.Result;
import fr.heavy.permission.group.Group;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class PermissionUser
{
    public static List<PermissionUser> users = new LinkedList<>();

    private PermissionAttachment attachment;
    private UUID uuid;
    private Group group;
    private List<String> extraPermissions;

    private PermissionUser(UUID uuid, Group group, List<String> extraPermissions)
    {
        this.uuid = uuid;
        this.group = group;
        this.extraPermissions = extraPermissions;
    }

    public Group getGroup()
    {
        return group;
    }

    public void setGroup(Group group)
    {
        this.group = group;
    }

    public List<String> getExtraPermissions()
    {
        return extraPermissions;
    }

    public static PermissionUser getUserByUUID(UUID uuid)
    {
        for (PermissionUser permissionUser : users)
        {
            if (permissionUser.uuid.equals(uuid))
                return permissionUser;
        }

        return null;
    }


    public void applyPermissions(Player player) {
        List<String> perms = new LinkedList<>(extraPermissions);
        perms.addAll(group.getPermissions());

        attachment = player.addAttachment(PermissionPlugin.getInstance());

        for (String permission : perms)
        {
            if (permission.startsWith("-"))
            {
                permission = permission.substring(1);
                attachment.setPermission(permission, false);
            } else
            {
                attachment.setPermission(permission, true);
            }
        }

        player.recalculatePermissions();
    }

    public void removePermissions(Player player) {
        player.removeAttachment(attachment);
    }

    public void updateGroupDatabase() {
        Database.executeUpdate("UPDATE tb_group SET grp_group = '%s' WHERE grp_uuid = '%s'", group.getName(), uuid);
    }


    public static PermissionUser loadUser(UUID uuid) {
        List<Result> results = Database.executeQuery("SELECT grp_group FROM tb_group WHERE grp_uuid = '%s'", uuid);
        List<Result> extraPermissions = Database.executeQuery("SELECT perm_permission FROM tb_permission WHERE perm_uuid = '%s'", uuid);

        Group group = Group.getDefaultGroup();
        if(results == null) {
            Database.executeUpdate("INSERT INTO tb_group(grp_uuid, grp_group) VALUES('%s', '%s')", uuid, group.getName());
        }
        else
        {
            group = Group.getByName(results.get(0).getAsString("grp_group"));
        }

        List<String> permissionsList = new LinkedList<>();

        if(extraPermissions != null) {
            extraPermissions.forEach(extraPermission -> {
                permissionsList.add(extraPermission.getAsString("perm_permission"));
            });
        }

        PermissionUser permissionUser = new PermissionUser(uuid, group, permissionsList);
        PermissionUser.users.add(permissionUser);

        return permissionUser;
    }
}
