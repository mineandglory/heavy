package fr.heavy.permission.commands;

import fr.heavy.core.api.translation.Translation;
import fr.heavy.core.api.utils.SelectorUtil;
import fr.heavy.permission.PermissionUser;
import fr.heavy.permission.group.Group;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.List;

public class GroupCommand implements CommandExecutor
{
    private String permission = "permission.group";

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (!sender.hasPermission(permission))
        {
            sender.sendMessage(Translation.getTranslation(sender, "noPermission", permission));

            return false;
        }

        if (args.length < 2 || args.length > 3)
        {
            sender.sendMessage(Translation.getTranslation(sender, "permissionCommandGroupUsage"));
            return false;
        }

        List<Entity> entityList = SelectorUtil.parseSelector(args[1], sender, "type=player");


        switch (args[0].toLowerCase())
        {
            case "info":
                for (Entity entity : entityList)
                {
                    PermissionUser permissionUser = PermissionUser.getUserByUUID(entity.getUniqueId());

                    sender.sendMessage(Translation.getTranslation(sender, "permissionUserGroupIs", entity.getName(), permissionUser.getGroup().getName()));
                }
                break;
            case "set":
                if (args.length != 3)
                {
                    sender.sendMessage(Translation.getTranslation(sender, "permissionCommandGroupUsage"));
                    return false;
                }

                Group group = Group.getByName(args[2]);

                if(group == null) {
                    sender.sendMessage(Translation.getTranslation(sender, "permissionUnknownGroup"));
                    return false;
                }

                if(sender instanceof Player) {
                    PermissionUser permissionSender = PermissionUser.getUserByUUID(((Player) sender).getUniqueId());

                    if (group.getWeight() > permissionSender.getGroup().getWeight())
                    {
                        sender.sendMessage(Translation.getTranslation(sender, "permissionGroupTooHigh"));
                        return false;
                    }

                }

                for (Entity entity : entityList)
                {
                    PermissionUser permissionUser = PermissionUser.getUserByUUID(entity.getUniqueId());
                    permissionUser.setGroup(group);

                    permissionUser.updateGroupDatabase();

                    entity.sendMessage(Translation.getTranslation(sender, "permissionYourGroupIsNow", group.getName()));
                    sender.sendMessage(Translation.getTranslation(sender, "permissionUserGroupIsNow", entity.getName(), group.getName()));
                }
                break;
            default:
                sender.sendMessage(Translation.getTranslation(sender, "permissionCommandGroupUsage"));
                return false;
        }
        return true;
    }
}
