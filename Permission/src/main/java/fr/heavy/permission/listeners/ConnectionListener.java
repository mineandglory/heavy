package fr.heavy.permission.listeners;

import fr.heavy.permission.PermissionUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener
{
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        PermissionUser user = PermissionUser.loadUser(event.getPlayer().getUniqueId());
        user.applyPermissions(event.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        PermissionUser permissionUser = PermissionUser.getUserByUUID(event.getPlayer().getUniqueId());

        permissionUser.removePermissions(event.getPlayer());
        PermissionUser.users.remove(permissionUser);
    }
}
