package fr.heavy.economy.listeners;

import fr.heavy.economy.EconomyUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener
{
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        EconomyUser.loadUser(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
       EconomyUser economyUser = EconomyUser.getUserByUUID(event.getPlayer().getUniqueId());
       economyUser.updateMoneyDatabase();

       EconomyUser.users.remove(economyUser);
    }
}
