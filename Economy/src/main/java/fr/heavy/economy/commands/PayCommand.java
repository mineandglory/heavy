package fr.heavy.economy.commands;

import fr.heavy.core.api.translation.Translation;
import fr.heavy.core.api.utils.SelectorUtil;
import fr.heavy.economy.EconomyPlugin;
import fr.heavy.economy.EconomyUser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.List;

public class PayCommand implements CommandExecutor
{

    private String permission = "economy.pay";

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!sender.hasPermission(permission)) {
            sender.sendMessage(Translation.getTranslation(sender, "noPermission", permission));

            return false;
        }

        if(!(sender instanceof Player)) return false;

        if (args.length != 2)
        {
            sender.sendMessage(Translation.getTranslation(sender, "economyCommandPayUsage"));
            return false;
        }

        double amount;
        try
        {
            amount = Double.parseDouble(args[args.length - 1]);
        } catch (NumberFormatException e)
        {
            sender.sendMessage(Translation.getTranslation(sender, "economyCommandPayUsage"));
            return false;
        }

        EconomyUser senderUser = EconomyUser.getUserByUUID(((Player) sender).getUniqueId());

        if(amount < EconomyPlugin.getMinAmount()) {
            sender.sendMessage(Translation.getTranslation(sender, "economyAmountTooLow"));
            return false;
        }

        if(amount > senderUser.getMoney()) {
            sender.sendMessage(Translation.getTranslation(sender, "economyAmountTooHigh"));
            return false;
        }

        List<Entity> entityList;

        entityList = SelectorUtil.parseSelector(args[0], sender, "type=player");

        for (Entity entity : entityList)
        {
            EconomyUser economyUser = EconomyUser.getUserByUUID(entity.getUniqueId());

            economyUser.giveMoney(amount);
            senderUser.takeMoney(amount);

            entity.sendMessage(Translation.getTranslation(sender, "economyHasPaidYou", sender.getName(), amount, EconomyPlugin.getCurrency()));
            entity.sendMessage(Translation.getTranslation(sender, "economyYouPaidPlayer", sender.getName(), amount, EconomyPlugin.getCurrency()));
        }

        return true;
    }
}
