package fr.heavy.economy.commands;

import fr.heavy.core.api.translation.Translation;
import fr.heavy.core.api.utils.SelectorUtil;
import fr.heavy.core.api.color.ChatColors;
import fr.heavy.core.api.color.Color;
import fr.heavy.economy.EconomyPlugin;
import fr.heavy.economy.EconomyUser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

public class EconomyCommand implements CommandExecutor
{

    private String permission = "economy.admin";

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!sender.hasPermission(permission)) {
            sender.sendMessage(Translation.getTranslation(sender, "noPermission", permission));

            return false;
        }

        if (args.length < 2 || args.length > 3)
        {
            sender.sendMessage(Translation.getTranslation(sender, "economyCommandEconomyUsage"));
            return false;
        }

        double amount;
        try
        {
            amount = Double.parseDouble(args[args.length - 1]);
        } catch (NumberFormatException e)
        {
            sender.sendMessage(Translation.getTranslation(sender, "economyCommandEconomyUsage"));
            return false;
        }

        List<Entity> entityList;
        if (args.length == 2)
        {
            if (!(sender instanceof Player)) return false;

            entityList = new LinkedList<>();
            entityList.add((Entity) sender);
        } else
        {
            entityList = SelectorUtil.parseSelector(args[1], sender, "type=player");
        }

        switch (args[0].toLowerCase())
        {
            case "give":
                for (Entity entity : entityList)
                {
                    EconomyUser economyUser = EconomyUser.getUserByUUID(entity.getUniqueId());

                    economyUser.giveMoney(amount);
                    entity.sendMessage(Translation.getTranslation(sender, "economyBalanceGive", amount, EconomyPlugin.getCurrency()));
                    sender.sendMessage(Translation.getTranslation(sender, "economyBalanceUserIsNow", entity.getName(), amount, EconomyPlugin.getCurrency()));
                }
                break;
            case "take":
                for (Entity entity : entityList)
                {
                    EconomyUser economyUser = EconomyUser.getUserByUUID(entity.getUniqueId());

                    economyUser.takeMoney(amount);
                    entity.sendMessage(Translation.getTranslation(sender, "economyBalanceTake", amount, EconomyPlugin.getCurrency()));
                    sender.sendMessage(Translation.getTranslation(sender, "economyBalanceUserIsNow", entity.getName(), amount, EconomyPlugin.getCurrency()));
                }
                break;
            case "set":
                for (Entity entity : entityList)
                {
                    EconomyUser economyUser = EconomyUser.getUserByUUID(entity.getUniqueId());

                    economyUser.setMoney(amount);
                    entity.sendMessage(Translation.getTranslation(sender, "economyBalanceNow", amount, EconomyPlugin.getCurrency()));
                    sender.sendMessage(Translation.getTranslation(sender, "economyBalanceUserIsNow", entity.getName(), amount, EconomyPlugin.getCurrency()));
                }
                break;
            default:
                sender.sendMessage(Translation.getTranslation(sender, "economyCommandEconomyUsage"));
                return false;
        }
        return true;
    }
}
