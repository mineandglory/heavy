package fr.heavy.economy.commands;

import fr.heavy.core.api.translation.Translation;
import fr.heavy.core.api.utils.SelectorUtil;
import fr.heavy.economy.EconomyPlugin;
import fr.heavy.economy.EconomyUser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

public class BalanceCommand implements CommandExecutor
{
    private String permission = "economy.balance";

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (!sender.hasPermission(permission))
        {
            sender.sendMessage(Translation.getTranslation(sender, "noPermission", permission));

            return false;
        }

        if (args.length > 1)
        {
            sender.sendMessage(Translation.getTranslation(sender, "economyCommandBalanceUsage"));
            return false;
        }

        List<Entity> entityList;
        if (args.length == 0)
        {
            if (!(sender instanceof Player)) return false;

            entityList = new LinkedList<>();
            entityList.add((Entity) sender);
        } else
        {
            entityList = SelectorUtil.parseSelector(args[0], sender, "type=player");
        }

        for (Entity entity : entityList)
        {
            EconomyUser economyUser = EconomyUser.getUserByUUID(entity.getUniqueId());

            entity.sendMessage(Translation.getTranslation(sender, "economyBalanceIs", entity.getName(), economyUser.getMoney(), EconomyPlugin.getCurrency()));
        }

        return true;
    }
}
