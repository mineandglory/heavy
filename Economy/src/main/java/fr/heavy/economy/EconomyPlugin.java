package fr.heavy.economy;

import fr.heavy.core.api.config.Config;
import fr.heavy.core.api.database.Database;
import fr.heavy.core.api.translation.Translation;
import fr.heavy.economy.commands.BalanceCommand;
import fr.heavy.economy.commands.EconomyCommand;
import fr.heavy.economy.commands.PayCommand;
import fr.heavy.economy.listeners.ConnectionListener;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class EconomyPlugin extends JavaPlugin
{
    private static String currency = "$";
    private static double minMoney = 0;
    private static double maxMoney = -1;
    private static double minAmount = 0.01;

    private static JavaPlugin instance;

    public static JavaPlugin getInstance()
    {
        return instance;
    }

    public static String getCurrency()
    {
        return currency;
    }

    public static double getMinMoney()
    {
        return minMoney;
    }

    public static double getMaxMoney()
    {
        return maxMoney;
    }

    public static double getMinAmount()
    {
        return minAmount;
    }

    @Override
    public void onEnable()
    {
        super.onEnable();
        instance = this;

        createDefaultConfig();

        Translation.loadTranslation(this);
        loadConfig();
        createDefaultDatabase();

        restoreUsers();
        scheduleUpdate();

        getServer().getPluginManager().registerEvents(new ConnectionListener(), this);

        getCommand("economy").setExecutor(new EconomyCommand());
        getCommand("balance").setExecutor(new BalanceCommand());
        getCommand("pay").setExecutor(new PayCommand());
    }

    private void scheduleUpdate()
    {
        int delay = 10 * 60 * 20; // 10 min

        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                for (EconomyUser economyUser : EconomyUser.users)
                {
                    economyUser.updateMoneyDatabase();
                }
            }
        }.runTaskTimer(this, delay, delay);
    }

    private void restoreUsers()
    {
        for (Player player : Bukkit.getOnlinePlayers())
        {
            EconomyUser.loadUser(player.getUniqueId());
        }
    }

    private void createDefaultDatabase()
    {
        Database.executeUpdate("CREATE TABLE IF NOT EXISTS tb_economy(" +
                "eco_uuid VARCHAR(36) PRIMARY KEY," +
                "eco_money FLOAT DEFAULT '%s'".replaceFirst("%s", String.valueOf(minMoney)) +
                ");");
    }

    private void createDefaultConfig()
    {
        if (!Config.configExists(this, "translations/EN_US.yml"))
            Config.saveResource(this, "EN_US.yml", "translations/EN_US.yml");

        if (!Config.configExists(this, "economy.yml"))
            Config.saveResource(this, "economy.yml", "economy.yml");
    }

    private void loadConfig()
    {
        YamlConfiguration economy = Config.getConfig(this, "economy");

        if (economy.contains("currency")) currency = economy.getString("currency");
        if (economy.contains("minMoney")) minMoney = economy.getDouble("minMoney");
        if (economy.contains("maxMoney")) maxMoney = economy.getDouble("maxMoney");
        if (economy.contains("minAmount")) minAmount = economy.getDouble("minAmount");
    }
}
