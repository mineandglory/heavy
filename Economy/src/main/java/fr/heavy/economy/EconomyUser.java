package fr.heavy.economy;

import fr.heavy.core.api.database.Database;
import fr.heavy.core.api.database.Result;
import fr.heavy.core.api.utils.NumberUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class EconomyUser
{
    public static List<EconomyUser> users = new LinkedList<>();

    private UUID uuid;
    private double money;

    private EconomyUser(UUID uuid, double money)
    {
        this.uuid = uuid;
        this.money = money;
    }

    public static EconomyUser getUserByUUID(UUID uuid)
    {
        for (EconomyUser economyUser : users)
        {
            if (economyUser.uuid.equals(uuid))
                return economyUser;
        }

        return null;
    }

    public double getMoney()
    {
        return money;
    }

    public void setMoney(double amount)
    {
        if (amount < EconomyPlugin.getMinMoney()) amount = EconomyPlugin.getMinMoney();
        if (EconomyPlugin.getMaxMoney() != -1 && amount > EconomyPlugin.getMaxMoney()) amount = EconomyPlugin.getMaxMoney();

        String minAmount = String.valueOf(EconomyPlugin.getMinAmount());
        int scale = minAmount.substring(minAmount.lastIndexOf(".")).length();

        amount = NumberUtil.scaleDown(amount, scale);

        money = amount;
    }

    public void giveMoney(double amount)
    {
        double newMoney = money + amount;

        setMoney(newMoney);
    }

    public void takeMoney(double amount)
    {
        double newMoney = money - amount;

        setMoney(newMoney);
    }

    public void updateMoneyDatabase() {
        Database.executeUpdate("UPDATE tb_economy SET eco_money = '%s' WHERE eco_uuid = '%s'", money, uuid);
    }

    public static void loadUser(UUID uuid) {
        List<Result> results = Database.executeQuery("SELECT * FROM tb_economy WHERE eco_uuid = '%s'", uuid);

        double money = EconomyPlugin.getMinMoney();
        if(results == null) {
            Database.executeUpdate("INSERT INTO tb_economy(eco_uuid) VALUES('%s')", uuid);
        }
        else
        {
            money = results.get(0).getAsDouble("eco_money");
        }

        EconomyUser economyUser = new EconomyUser(uuid, money);

        EconomyUser.users.add(economyUser);
    }
}
