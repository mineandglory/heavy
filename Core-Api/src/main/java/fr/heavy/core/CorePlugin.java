package fr.heavy.core;

import fr.heavy.core.api.config.Config;
import fr.heavy.core.api.database.Database;
import fr.heavy.core.api.nms.NMS;
import fr.heavy.core.api.placeholder.Placeholder;
import fr.heavy.core.api.translation.Locale;
import fr.heavy.core.api.translation.Translation;
import fr.heavy.core.api.utils.EnumUtil;
import fr.heavy.core.listeners.api.TranslationListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class CorePlugin extends JavaPlugin
{
    private static JavaPlugin instance;

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;

        createDefaultConfig();

        Translation.loadTranslation(this);
        Database.connectDatabase();
        restoreUsers();

        getServer().getPluginManager().registerEvents(new TranslationListener(), this);

        new Placeholder("displayname", Player::getDisplayName);
    }

    @Override
    public void onDisable() {
        super.onDisable();
        Database.disconnectDatabase();
    }

    private void restoreUsers()
    {
        for (Player player : Bukkit.getOnlinePlayers())
        {
            String nms = NMS.getLocale(player);
            Locale locale = Locale.EN_US;

            if (EnumUtil.contains(Locale.class, nms))
                locale = Locale.valueOf(nms);

            Translation.localeMap.put(player.getUniqueId(), locale);
        }
    }

    public static JavaPlugin getInstance()
    {
        return instance;
    }

    private void createDefaultConfig() {
        if (!Config.configExists(this, "translations/EN_US.yml"))
            Config.saveResource(this, "EN_US.yml", "translations/EN_US.yml");

        if (!Config.configExists(this, "database.yml"))
            Config.saveResource(this, "database.yml", "database.yml");
    }
}
