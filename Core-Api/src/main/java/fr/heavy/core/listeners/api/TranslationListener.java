package fr.heavy.core.listeners.api;

import fr.heavy.core.api.nms.NMS;
import fr.heavy.core.api.translation.Locale;
import fr.heavy.core.api.translation.Translation;
import fr.heavy.core.api.utils.EnumUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class TranslationListener implements Listener
{
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        String nms = NMS.getLocale(event.getPlayer());
        Locale locale = Locale.EN_US;

        if (EnumUtil.contains(Locale.class, nms))
            locale = Locale.valueOf(nms);

        Translation.localeMap.put(event.getPlayer().getUniqueId(), locale);
    }
}
