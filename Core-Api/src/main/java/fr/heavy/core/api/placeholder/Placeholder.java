package fr.heavy.core.api.placeholder;

import fr.heavy.core.api.translation.Locale;
import fr.heavy.core.api.translation.Translation;
import fr.heavy.core.api.utils.RegexUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class Placeholder
{
    static List<Placeholder> placeholders = new LinkedList<>();

    String name;
    Function<Player, String> function;

    public Placeholder(String name, Function<Player, String> function)
    {
        this.name = name;
        this.function = function;

        placeholders.add(this);
    }

    public String getValue(Player player) {
        return function.apply(player);
    }

    public static String computePlaceholder(Player player, String string)
    {
        List<String> regex = RegexUtil.getMatches("%([a-zA-Z_]+)%+", string);

        if (regex.isEmpty() ) return string;

        for (String ph : regex)
        {
            Placeholder placeholder = getByName(ph);
            if(placeholders == null) continue;

            string = string.replaceAll("%" + ph + "%", placeholder.getValue(player));
        }

        return string;
    }

    private static Placeholder getByName(String string) {
        for (Placeholder placeholder :placeholders)
        {
            if(placeholder.name.equalsIgnoreCase(string)) return placeholder;
        }

        return null;
    }
}
