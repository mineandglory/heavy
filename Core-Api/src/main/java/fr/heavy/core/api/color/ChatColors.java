package fr.heavy.core.api.color;

public enum ChatColors
{
    LIGHT_BLUE("#a6e3e9"),
    ERROR("#c90035"),

    WHITE("#FFFFFF");


    private String code;

    ChatColors(String code)
    {
        this.code = code;
    }


    @Override
    public String toString()
    {
        return Color.valueOf(code);
    }
}
