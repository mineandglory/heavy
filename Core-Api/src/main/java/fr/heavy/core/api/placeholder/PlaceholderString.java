package fr.heavy.core.api.placeholder;

import org.bukkit.entity.Player;

public class PlaceholderString
{
    String content;

    public PlaceholderString(String content)
    {
        this.content = content;
    }

    public String build(Player player)
    {
        for (Placeholder placeholder : Placeholder.placeholders)
        {
            String placeholderName = "%" + placeholder.name + "%";
            if (!content.contains(placeholderName)) continue;

            content = content.replaceAll(placeholderName, placeholder.getValue(player));
        }

        return content;
    }

}
