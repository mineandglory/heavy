package fr.heavy.core.api.utils;

import fr.heavy.core.CorePlugin;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

public class ItemUtil
{
    public static void setTag(ItemStack itemStack, String key, Object value) {
        NamespacedKey namespacedKey = new NamespacedKey(CorePlugin.getInstance(), key);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.getPersistentDataContainer().set(namespacedKey, PersistentDataType.STRING, value.toString());
        itemStack.setItemMeta(itemMeta);
    }

    public static String getTag(ItemStack itemStack, String key) {
        NamespacedKey namespacedKey = new NamespacedKey(CorePlugin.getInstance(), key);
        ItemMeta itemMeta = itemStack.getItemMeta();
        return itemMeta.getPersistentDataContainer().get(namespacedKey, PersistentDataType.STRING);
    }

    public static boolean hasTag(ItemStack itemStack, String key) {
        NamespacedKey namespacedKey = new NamespacedKey(CorePlugin.getInstance(), key);
        ItemMeta itemMeta = itemStack.getItemMeta();
        return itemMeta.getPersistentDataContainer().has(namespacedKey, PersistentDataType.STRING);
    }
}
