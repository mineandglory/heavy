package fr.heavy.core.api.config;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.*;

public class Config
{
    private static String pluginFolder = "plugins/Heavy";

    /**
     * Gets an YamlConfiguration of a desired file
     *
     * @param relativePath The relative path to the file
     * @return The config
     */
    public static YamlConfiguration getConfig(Plugin plugin, String relativePath)
    {
        try
        {
            String path = pluginFolder + "/" + plugin.getName() + "/" + relativePath;
            String folder = path.substring(0, path.lastIndexOf("/"));

            File folderFile = new File(folder);
            if (!folderFile.exists()) folderFile.mkdirs();

            YamlConfiguration config = new YamlConfiguration();

            File configFile = new File(path.replace(".yml", "") + ".yml");
            if (!configFile.exists()) return null;

            config.load(configFile);

            return config;
        } catch (IOException | InvalidConfigurationException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Saves a config to a given file name
     *
     * @param config       The config to save
     * @param relativePath The relative path to the file
     */
    public static void saveConfig(Plugin plugin, YamlConfiguration config, String relativePath)
    {
        try
        {
            String path = pluginFolder + "/" + plugin.getName() + "/" + relativePath;
            String folder = path.substring(0, path.lastIndexOf("/"));

            File folderFile = new File(folder);
            if (!folderFile.exists()) folderFile.mkdirs();

            File configFile = new File(path.replace(".yml", "") + ".yml");
            if (!configFile.exists()) configFile.createNewFile();

            config.save(configFile);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void saveResource(Plugin plugin, String file, String destination) {
       saveResource(plugin, file, destination, false);
    }

    public static void saveResource(Plugin plugin, String file, String destination, boolean replace) {
        try
        {
            String path = pluginFolder + "/" + plugin.getName() + "/" + destination;
            String folder = path.substring(0, path.lastIndexOf("/"));

            InputStream stream = plugin.getResource(file);
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);

            File folderFile = new File(folder);
            if (!folderFile.exists()) folderFile.mkdirs();

            File targetFile = new File(path);
            if(targetFile.exists() && !replace) return;

            OutputStream outStream = new FileOutputStream(targetFile);
            outStream.write(buffer);

            outStream.close();
            stream.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static boolean configExists(Plugin plugin, String relativePath)
    {
        String path = pluginFolder + "/" + plugin.getName() + "/" + relativePath.replace(".yml", "") + ".yml";

        return new File(path).exists();
    }
}
