package fr.heavy.core.api.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtil
{
    public static float scaleDown(float value, int scale)
    {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(scale, RoundingMode.DOWN);
        return bd.floatValue();
    }

    public static double scaleDown(double value, int scale)
    {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(scale, RoundingMode.DOWN);
        return bd.doubleValue();
    }
}
