package fr.heavy.core.api.color;

import fr.heavy.core.api.utils.RegexUtil;

import java.util.List;

public class Color
{
    public static String valueOf(int r, int g, int b) {
        String hex = String.format("%02x%02x%02x", r, g, b);
        return valueOf(hex);
    }

    public static String valueOf(String hex) {
        hex = hex.replace("#", "");
        hex = "x" + hex;

        return hex.replaceAll("(.)", "§$1");
    }

    public static String computeHex(String string) {
        List<String> regex = RegexUtil.getMatches("hex\\((\\#{0,1}[A-Fa-f0-9]{6})\\)", string);
        if(regex.isEmpty()) return string;

        for (String hex : regex) {
            string = string.replaceAll("hex\\(" + hex + "\\)", valueOf(hex));
        }

        return string;
    }

    public static String computeChatColors(String string) {
        List<String> regex = RegexUtil.getMatches("color\\(([A-Z_]+)\\)", string);
        if(regex.isEmpty()) return string;

        for (String color : regex) {
            string = string.replaceAll("color\\(" + color + "\\)", ChatColors.valueOf(color).toString());
        }

        return string;
    }
}
