package fr.heavy.core.api.nms;

import fr.heavy.core.CorePlugin;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NMS
{
    private static String getVersion(Server server) {
        final String packageName = server.getClass().getPackage().getName();

        return packageName.substring(packageName.lastIndexOf('.') + 1);
    }

    public static String getLocale(Player player) {
        try
        {
            String nms = "net.minecraft.server." + getVersion(CorePlugin.getInstance().getServer()) + ".";
            String obc = "org.bukkit.craftbukkit." +  getVersion(CorePlugin.getInstance().getServer()) + ".";

            Class classCraftPlayer = Class.forName(obc + "entity.CraftPlayer");
            Class classEntityPlayer = Class.forName(nms + "EntityPlayer");

            Method methodGetHandle = classCraftPlayer.getDeclaredMethod("getHandle");

            Object craftPlayer = classCraftPlayer.cast(player);
            Object entityPlayer = methodGetHandle.invoke(craftPlayer);

            Field locale = classEntityPlayer.getDeclaredField("locale");
            locale.setAccessible(true);

            return ((String) locale.get(entityPlayer)).toUpperCase();
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
