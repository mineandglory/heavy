package fr.heavy.core.api.translation;

import fr.heavy.core.api.color.Color;
import fr.heavy.core.api.config.Config;
import fr.heavy.core.api.placeholder.Placeholder;
import fr.heavy.core.api.utils.RegexUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Translation
{
    public static Map<UUID, Locale> localeMap = new HashMap<>();
    public static Map<Locale, Map<String, String>> translationMap = new HashMap<>();

    public static String getTranslation(CommandSender sender, String key, Object... values)
    {
        Locale locale;

        if (sender != null)
        {
            if (sender instanceof Player)
            {
                locale = localeMap.get(((Player) sender).getUniqueId());
                return getTranslation((Player) sender, locale, key, values);
            }
        }

        locale = Locale.EN_US;

        return getTranslation(null, locale, key, values);
    }

    private static String getTranslation(Player player, Locale locale, String key, Object... values)
    {
        String text = translationMap.get(locale).get(key);
        if (text == null) return "";

        text = text.replaceAll("\\\\n", "\n");
        text = text.replaceAll("\\\\t", "  ");
        text = text.replaceAll("\\\\2t", "    ");

        text = computeTranslation(player, locale, text);
        text = Placeholder.computePlaceholder(player, text);

        text = Color.computeHex(text);
        text = Color.computeChatColors(text);

        for (Object obj : values)
        {
            text = text.replaceFirst("%s", obj.toString().replaceAll("(.)", "\\\\$1"));
        }

        return text;
    }

    public static void loadTranslation(Plugin plugin)
    {
        for (Locale locale : Locale.values())
        {
            if (Config.configExists(plugin, "translations/" + locale))
            {
                YamlConfiguration config = Config.getConfig(plugin, "translations/" + locale);

                if (!translationMap.containsKey(locale)) translationMap.put(locale, new HashMap<>());

                for (String key : config.getKeys(false))
                {
                    translationMap.get(locale).put(key, config.getString(key));
                }
            }
        }
    }

    public static String computeTranslation(Player player, Locale locale, String string)
    {
        List<String> regex = RegexUtil.getMatches("translate\\(([a-zA-Z_]+)\\)", string);
        if (regex.isEmpty()) return string;

        for (String translate : regex)
        {
            string = string.replaceAll("translate\\(" + translate + "\\)", Translation.getTranslation(player, locale, translate));
        }

        return string;
    }
}
