package fr.heavy.core.api.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil
{
    public static boolean matching(String regex, String str)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        return matcher.find();
    }

    public static List<String> getMatches(String regex, String str)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        List<String> result = new LinkedList<>();

        while (matcher.find()) {
            for (int i = 1; i < matcher.groupCount() + 1; i++)
            {
                result.add(matcher.group(i));
            }
        }

        return result;
    }
}
