package fr.heavy.core.api.translation;

public enum Locale
{
    EN_US,
    FR_FR
}
