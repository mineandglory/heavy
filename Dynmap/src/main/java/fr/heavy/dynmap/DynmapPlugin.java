package fr.heavy.dynmap;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapCommonAPI;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.Marker;
import org.dynmap.markers.MarkerSet;

public class DynmapPlugin extends JavaPlugin
{
    @Override
    public void onEnable()
    {
        super.onEnable();
        double[] x = {-100, -100, 100, 100};
        double[] z = {-100, 100, 100, -100};

        double[] x1 = {-100, -100, -200, -200};
        double[] z1 = {-100, -200, -200, -100};


        DynmapCommonAPI dynmap = (DynmapCommonAPI) Bukkit.getServer().getPluginManager().getPlugin("dynmap");
        MarkerSet set = dynmap.getMarkerAPI().createMarkerSet("heavy.claims", "Claims", null, false);

        AreaMarker m = set.createAreaMarker("world_0", "Spawn", true, "world", x, z, false);
        m.setFillStyle(0.35, Integer.parseInt("#00CC00".substring(1), 16));
        m.setLineStyle(3, 0.8, Integer.parseInt("#00CC00".substring(1), 16));
        m.setBoostFlag(true);
        m.setDescription("<h1>Spawn</h1><h2 style=\"font-weight: normal;\">Zone de départ de tous les joueurs</h2><h3>Tags:</h3><pre>PVP: No\nInvincibility: Yes\nNoHunger: Yes\nBuild: No\nMobSpawn: No\nAnimalSpawn: No</pre>");

        AreaMarker m2 = set.createAreaMarker("world_1", "Claim0", true, "world", x1, z1, false);
        m2.setFillStyle(0.35, Integer.parseInt("#000066".substring(1), 16));
        m2.setLineStyle(3, 0.8, Integer.parseInt("#000066".substring(1), 16));
        m2.setBoostFlag(true);
        m2.setDescription("<h1>Claim</h1><h3>Owner: Fingarde</h3><h3>Trusted players:</h3><pre>YobiNoShio</pre>");

    }
}
