package fr.heavy.item;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.LinkedList;
import java.util.List;

public class Item
{
    private static List<Item> items = new LinkedList<>();

    private String localizedName;
    private Material material;
    private int customModelData;
    private String title;
    private String[] data;

    public Item(String localizedName, Material material, int customModelData, String title, String... data)
    {
        this.localizedName = localizedName;
        this.material = material;
        this.customModelData = customModelData;
        this.title = title;


        items.add(this);
    }

    private Item(String localizedName)
    {
        this.localizedName = localizedName;
    }

    public static Item getFromItem(ItemStack itemStack)
    {
        if (itemStack == null || itemStack.getItemMeta() == null) return null;
        if (itemStack.getItemMeta().getLocalizedName().equals("")) return null;

        return getFromName(itemStack.getItemMeta().getLocalizedName());
    }

    public static Item getFromName(String localizedName)
    {
        for (Item item : items)
        {
            if(item.localizedName.equals(localizedName)) return item;
        }

        return null;
    }

    public ItemStack getItem()
    {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName("§r" + title);
        meta.setCustomModelData(customModelData);

        meta.setLocalizedName(localizedName);
        item.setItemMeta(meta);
        return item;
    }

    public String[] getData()
    {
        return data;
    }

    @Override
    public int hashCode()
    {
        return localizedName.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Item item = (Item) obj;

        return item.localizedName.equals(this.localizedName);
    }

}
